// Copywrite 2021 James Hemsing
// GPLv2 License
#ifndef GRAPHPAINTER_H
#define GRAPHPAINTER_H

#include <QWidget>
#include <QGraphicsView>
class dataServer;
class QImage;
class graphPainter : public QGraphicsView
{
    Q_OBJECT
public:
    explicit graphPainter(QWidget *parent = nullptr);

    void setDataServer(dataServer *);

    void drawSpectrumToImage(float * floatData, int npts);
protected:
    void paintEvent(QPaintEvent *event) override;

private:
    dataServer * mDataServer;
    QImage * mGraph;
signals:

public slots:
    void updateGraph();

};

#endif // GRAPHPAINTER_H
