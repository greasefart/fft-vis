
// Copywrite 2021 James Hemsing
// GPLv2 License

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <Q3DSurface>
#include <QHeightMapSurfaceDataProxy>

using namespace QtDataVisualization;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
class dataServer;
class graphPainter;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void startServer();
    void stopServer();

    void printConsole(QString);
    void updateSurface(float *);
    void clearSurface();
    void toggleSurfaceUpdate();

private:
    Ui::MainWindow *ui;

    dataServer * mDataServer;

    graphPainter * mGraphPainter;

    bool mSurfaceUpdateEnable;

    Q3DSurface  *mSurface;
    QSurfaceDataProxy * mProxy;
    //QHeightMapSurfaceDataProxy * mProxy;
    QSurface3DSeries * mSeries;

    QSurfaceDataArray * mArray;

    unsigned int mNumSpectra;
};
#endif // MAINWINDOW_H
