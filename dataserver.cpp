// Copywrite 2021 James Hemsing
// GPLv2 License
#include "dataserver.h"
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QTcpSocket>
#include <QDataStream>
#include <QCoreApplication>
#include <iostream>
#include "graphpainter.h"
#include <QThread>
#include <QTime>


uint32_t * generate_reversed_order_indices(uint32_t num_bits)
{
  //const int debug = 0;
  uint32_t total = (1 << num_bits); // total number of indices to generate
  uint32_t i, j, t, res;
  fprintf(stdout, "num_bits: %u, total: %u\n", num_bits, total);

  uint32_t * indices = (uint32_t *)malloc( sizeof(uint32_t)*total ) ;

  if (!indices) {
    return NULL;
  }


  for (i = 0; i < total; i++)
  {
    //if (debug) printf("i: %u\n", i);
    res = 0;
    for (j = 0; j < num_bits; j++)
    {
      t = (i >> j) & 0x1; // peel off the LSB
      res |= (t << (num_bits - 1 - j)); // put the LSB in the jth position from the left
      //if (debug) printf("%u ", t);
    }
    //printf("%u:%u,", i, res);
    //indices[total - i - 1] = res;
    indices[i] = res;
  }

  printf("\nnum_points: %u\n", i);
  fflush(stdout);

  return indices;
}

void dataServer::init(uint32_t npts)
{
    if (mFloatData) free (mFloatData);

    mNumPoints = npts;

    mFloatData = (float*)malloc(mNumPoints * sizeof(float));

    mReversedIndices = generate_reversed_order_indices((uint32_t)log2f((double)mNumPoints));
}

dataServer::dataServer(QObject *parent, graphPainter * aPainter)
{
    mPainter = aPainter;
    mDebug = false;
    connect(this, SIGNAL(writeText(QString)), parent, SLOT(printConsole(QString)));
    connect(this, SIGNAL(clearSurface()), parent, SLOT(clearSurface()));

    mNumPoints = 0;
    mFloatData = nullptr;
}

/*
void dataServer::start()
{
}
*/

void dataServer::run()
{
    //mTcpServer = new QTcpServer(this);
    mServerEnable = true;
    mTcpServer = new QTcpServer();
    int res = mTcpServer->listen(QHostAddress::Any, 9000);

    if (!res) {
        std::cout << "Server not started" << std::endl;
        mStatus = -1;
    }

    //std::cout << "listening on port " << mTcpServer->serverPort() << std::endl;
    QString str;
    str += "listening on port " + QString::number(mTcpServer->serverPort()) ;

    emit writeText(str);

    str = "Waiting for connection...";
    emit writeText(str);

    QCoreApplication::processEvents();

    // this is used in single threaded
    //connect(mTcpServer, &QTcpServer::newConnection, this, &dataServer::getData);
    mStatus = 1;
    while (mServerEnable) getData();
    mStatus = 0;

    mTcpServer->close();
    delete mTcpServer;
    mTcpServer = nullptr;

}

void dataServer::getData()
{
    mTcpServer->waitForNewConnection(-1);
    uint32_t i;
    // Signal invoked by new socket connection
    QTcpSocket * tSocket = mTcpServer->nextPendingConnection();
    if (!tSocket) {
        emit writeText("Socket not ready");
        return;
    }

    connect(tSocket, &QAbstractSocket::disconnected,
            tSocket, &QObject::deleteLater);

    // Write 's' to tell the client we're ready
    tSocket->write("s", 1);
    tSocket->waitForBytesWritten();

    // Wait for 4 bytes from client containing the number of FFT points and channel
    if (!tSocket->waitForReadyRead(5000)) {
        emit writeText("Did not receive NPTS in time");
        tSocket->close();
        return;
    }

    while (tSocket->bytesAvailable() < 4)
        emit writeText("less than 4 bytes available");

    //
    // Receive number of points from client and resize things
    //
    char tIntArr [4];
    QByteArray tNpts = tSocket->read(4);
    tIntArr[0] = tNpts.at(0);
    tIntArr[1] = tNpts.at(1);
    tIntArr[2] = tNpts.at(2);
    tIntArr[3] = tNpts.at(3);
    uint32_t tNumPts;
    memcpy((void *)&tNumPts, &tIntArr, 4);

    // if the client is now sending a different size FFT, reset the surface
    if (tNumPts != mNumPoints) {
        emit clearSurface();
        init(tNumPts);
    }

    int byte_size = mNumPoints*sizeof(float);

    //
    // write 'a' for acknowledge back to the client for additional help synchronizing
    //
    tSocket->write("a", 1);
    tSocket->waitForBytesWritten();

    //
    // Begin receiving the FFT data with ample handshaking for compensate for flaky network.
    // The bytes sent per loop iteration is configured by the client.
    //
    const int tries = 100;
    int to = 0;

    int32_t bytes_avail;
    QByteArray tArray;

    while (tArray.size() < byte_size && to < tries)
    {
        if (!tSocket->waitForReadyRead(500)) {
            // not needed in separate thread
            //QCoreApplication::processEvents(/*QEventLoop::ExcludeUserInputEvents*/);
            to ++;
            continue;
        }
        bytes_avail = tSocket->bytesAvailable();
        QByteArray tba = tSocket->read(bytes_avail);
        //emit writeText(QString::number(to) + ": bytes received: " + QString::number(bytes_avail));

        if (tba.size() != bytes_avail)
            emit writeText("bytes avail: " + QString::number(bytes_avail) + " bytes received: " + QString::number(tba.size()));
        tArray.append(tba);
        int wr = tSocket->write( (char *) &bytes_avail, sizeof(bytes_avail) );
        if (wr < 4) emit writeText("wrote: " + QString::number(wr));
        tSocket->waitForBytesWritten();
        to++;
    }

    if (to >= tries) {
        emit writeText("Connection failed in the middle of transfer.");
        tSocket->close();
        return;
    }

    tSocket->write("e", 1);
    tSocket->waitForBytesWritten();
    //QCoreApplication::processEvents(/*QEventLoop::ExcludeUserInputEvents*/);
    float f;
    //QString fStr;
    //const float floor = -150.0f;
    char floatStr [4];
    const int phase_reorder = 0; // this is being done in the GUI rendering
    unsigned int t;
    for (i = 0; i < mNumPoints; i++) {
        if (phase_reorder) {
            if (i >= mNumPoints/2) t = i - (mNumPoints/2);
            else t = i + (mNumPoints/2);
        } else {
            t = i;
        }

        //t = i;
        floatStr[0] = tArray.at(4*t);
        floatStr[1] = tArray.at(4*t+1);
        floatStr[2] = tArray.at(4*t+2);
        floatStr[3] = tArray.at(4*t+3);

        memcpy((void*)&f, floatStr, sizeof(float));
        //fStr += QString::number(i) + ":" + (QString::number(f)) + ",";

        // Use prepopulated reversed bit order indices caused by FFT
        mFloatData[mReversedIndices[t]] = 10.0*logf(f/50.0);

        /*
        if (mDebug) {
            fStr = QString::number(f);
            emit writeText(fStr);
        }
        */
    }


    emit writeText(QString::number(mNumPoints) + " points received @ " + QTime::currentTime().toString());

    //emit writeText(fStr);
    mPainter->drawSpectrumToImage(mFloatData, mNumPoints);
    emit updateGraph(mFloatData);

    QCoreApplication::processEvents(/*QEventLoop::ExcludeUserInputEvents*/);
}

int dataServer::stop()
{
    mServerEnable = false;
    return mStatus;
}


float * dataServer::getFloatData()
{
    return mFloatData;
}

