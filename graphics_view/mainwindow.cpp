#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "graphics_item.h"
#include "timer_thread.h"
#include <QLayout>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QPainter>
#include <QObject>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QGraphicsScene * scene = new QGraphicsScene;
    scene->setSceneRect(QRectF(300,200,000,10));

    square = new graphics_item(this);

    scene->addItem(square);

    QGraphicsView * view = new QGraphicsView(scene);
    view->setDragMode(QGraphicsView::ScrollHandDrag);
    //view->setRenderHint(QPainter::Antialiasing);
    //view->show();

    ui->centralwidget->layout()->addWidget(view);

    m_timer = new timer_thread(this);


    QObject::connect(m_timer, &timer_thread::send_data, this, &MainWindow::update_data);

    m_timer->start();
}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update_data(int * data)
{
    this->data = data;
    square->update();
}

int * MainWindow::get_data()
{
    return data;
}
