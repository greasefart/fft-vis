#include "graphics_item.h"
#include <QPainter>
#include <QWidget>
#include <iostream>
#include <QRandomGenerator>
#include "mainwindow.h"

graphics_item::graphics_item(MainWindow * aWindow)
{
    mW = 400;
    mH = 400;
    mWindow = aWindow;
}

QPainterPath graphics_item::shape() const
{
    QPainterPath path;
    path.addRect(0, 0, mW, mH);
    return path;
}

QRectF graphics_item::boundingRect() const
{
    //interesting
    return QRectF (QPointF(0,0), QPointF(mW, mH));
}

void graphics_item::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *widget)
{
    int * data = mWindow->get_data();
    if (!data)
        return;

    mW = widget->width();
    mH = widget->height();
    //std::cout << w  << "x" << h << std::endl;

    QColor blue (Qt::blue);
    painter->setPen(blue);

    int i, y;
    for (i = 0; i < mW; i++)
    {
        //y = rand.bounded(mH);
        y = data[i];
        painter->drawLine(mapToScene(i, mH), mapFromScene(i, mH-y));
    }

    QColor red (Qt::red);
    painter->setPen(red);
    QLineF line (QPointF(0, 0), mapFromScene(100, 100));
    painter->drawLine(line);
    painter->drawText(QPointF(0,0), "farts");

}
