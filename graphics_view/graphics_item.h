#ifndef GRAPHICS_ITEM_H
#define GRAPHICS_ITEM_H

#include <QGraphicsItem>

class MainWindow;
class graphics_item : public QGraphicsItem
{
public:
    graphics_item(MainWindow *);
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;


private:
    int npts;
    int mW;
    int mH;

    MainWindow * mWindow;
};

#endif // GRAPHICS_ITEM_H
